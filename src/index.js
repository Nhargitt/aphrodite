import {defaultSelectorHandlers} from './generate';
import makeExports from './exports';

const useImportant = true; // Add !important to all style definitions
const amp = false;
export default makeExports(
    useImportant,
    defaultSelectorHandlers
);
