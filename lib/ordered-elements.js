
/* global Map */

"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var OrderedElements = (function () {
    /* ::
    elements: {[string]: any};
    keyOrder: string[];
     static fromObject: ({[string]: any}) => OrderedElements;
    static fromMap: (Map<string,any>) => OrderedElements;
    static from: (Map<string,any> | {[string]: any} | OrderedElements) =>
        OrderedElements;
    */

    function OrderedElements() {
        var elements /* : {[string]: any} */ = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
        var keyOrder /* : string[] */ = arguments.length <= 1 || arguments[1] === undefined ? [] : arguments[1];

        _classCallCheck(this, OrderedElements);

        this.elements = elements;
        this.keyOrder = keyOrder;
    }

    _createClass(OrderedElements, [{
        key: "forEach",
        value: function forEach(callback /* : (string, any) => void */) {
            for (var i = 0; i < this.keyOrder.length; i++) {
                callback(this.keyOrder[i], this.elements[this.keyOrder[i]]);
            }
        }
    }, {
        key: "map",
        value: function map(callback /* : (string, any) => any */) /* : OrderedElements */{
            var results = new OrderedElements();
            for (var i = 0; i < this.keyOrder.length; i++) {
                results.set(this.keyOrder[i], callback(this.keyOrder[i], this.elements[this.keyOrder[i]]));
            }
            return results;
        }
    }, {
        key: "set",
        value: function set(key, /* : string */value /* : any */) {
            if (!this.elements.hasOwnProperty(key)) {
                this.keyOrder.push(key);
            }
            this.elements[key] = value;
        }
    }, {
        key: "get",
        value: function get(key /* : string */) /* : any */{
            return this.elements[key];
        }
    }, {
        key: "has",
        value: function has(key /* : string */) /* : boolean */{
            return this.elements.hasOwnProperty(key);
        }
    }]);

    return OrderedElements;
})();

exports["default"] = OrderedElements;

OrderedElements.fromObject = function (obj) {
    return new OrderedElements(obj, Object.keys(obj));
};

OrderedElements.fromMap = function (map) {
    var ret = new OrderedElements();
    map.forEach(function (val, key) {
        ret.set(key, val);
    });
    return ret;
};

OrderedElements.from = function (obj) {
    if (obj instanceof OrderedElements) {
        // NOTE(emily): This makes a shallow copy of the previous elements, so
        // if the elements are deeply modified it will affect all copies.
        return new OrderedElements(_extends({}, obj.elements), obj.keyOrder.slice());
    } else if (
    // For some reason, flow complains about a plain
    // `typeof Map !== "undefined"` check. Casting `Map` to `any` solves
    // the problem.
    typeof /*::(*/Map /*: any)*/ !== "undefined" && obj instanceof Map) {
        return OrderedElements.fromMap(obj);
    } else {
        return OrderedElements.fromObject(obj);
    }
};
module.exports = exports["default"];